RSS For Yii2 Framework
=============================
Create RSS use by array or database query

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist winkers/yii2-rss "dev-master"
```

or add

```
"winkers/yii2-rss": "dev-master"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
        $rss = new \winkers\rss\Rss();

        $channel = [];
        $channel['title'] = 'My Title';
        $channel['link'] = 'http://example.com';
        $channel['description'] = 'The description will be come here.';
        $channel['language'] = 'en-us';

        /**
         * Get data by query for RSS uses
         */
        $data = (new \yii\db\Query())->select(['title', 'description'])->from('table')->all();

        /**
         * Get data by array for RSS uses
         */
        $data = [
            [
                'title' => 'My Title',
                'description' => 'The description will be come here.',
            ],
        ];

        $channel['items'] = [];

        foreach ($data as $k => $v) {
            $channel['items'][] = [
                'title' => $v['title'],
                'description' => substr($v['content'], 0, 500),
            ];
        }
        header('Content-Type: text/xml');
        echo $rss->createFeed($channel);
```

Notice that one `query` or `array` way should use.
